<?php


///////////////////scalar data type example////////////////
//bolean data types example
    $myTestVariable = true;

    var_dump($myTestVariable);

echo "<br>";

// Integer data type example

    $myTestVariable = 56;
    var_dump($myTestVariable);

echo "<br>";

//float data type example

$myTestVariable = 5.96;
var_dump($myTestVariable);

echo "<br>";

//string data type example

$myTestVariable = "Hello PHP World";
var_dump($myTestVariable);



/////////////////compound data type example/////////////////

//Array

    //Index Array
    echo "<br>";
$person = array(

            array("Kuddus", 44, 5.8);
            array("Abul", 34, 6.8);
            array("Kabul", 54, 4.8);
            array("Habla", 46, 5.1);
            array("Kabla", 43, 5.6);
)


    echo $person [2][0] . " " . $person[2][1] . " " . $person[2][2];